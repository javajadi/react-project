import React from 'react'
import {render} from 'react-dom'
import injectSheet from 'react-jss'

{
    "body": {
      "padding": "20px",
      "margin": "0",
      "fontFamily": "\"Roboto\", \"Helvetica Neue\", Helvetica, Arial, sans-serif",
      "fontSize": "14px",
      "lineHeight": "1.42857143",
      "color": "#3e3f3a",
      "backgroundColor": "#ffffff"
    },
    "h1": {
      "fontFamily": "Helvetica, Arial, sans-serif"
    },
    "h2": {
      "fontFamily": "Helvetica, Arial, sans-serif"
    },
    "p": {
      "fontFamily": "Helvetica, Arial, sans-serif"
    },
    "ul": {
      "fontFamily": "Helvetica, Arial, sans-serif"
    },
    "li": {
      "fontFamily": "Helvetica, Arial, sans-serif"
    },
    "ul_header_li": {
      "display": "inline",
      "listStyleType": "none",
      "margin": "0"
    },
    "ul_header": {
      "backgroundColor": "#333333",
      "padding": "0",
      "overflow": "hidden"
    },
    "ul_header_li_a": {
      "color": "#FFF",
      "fontWeight": "bold",
      "textDecoration": "none",
      "padding": "20px",
      "display": "inline-block"
    },
    "content": {
      "backgroundColor": "#AAA5A5",
      "padding": "20px",
      "margin": "18px 24px 0 10px",
      "textAlign": "left"
    },
    "content_h2": {
      "padding": "0",
      "margin": "0"
    },
    "content_li": {
      "marginBottom": "10px"
    },
    "active": {
      "background": "#008CBA"
    },
    "menuwrapper_ul": {
      "listStyleType": "none",
      "margin": "0 24px 0 10px",
      "marginTop": "18px",
      "padding": "0",
      "overflow": "hidden",
      "backgroundColor": "#333333",
      "fontSize": "0.8125rem"
    },
    "menuwrapper_li": {
      "float": "left"
    },
    "menuwrapper_li_a": {
      "display": "block",
      "color": "white",
      "textAlign": "center",
      "padding": "14px 16px",
      "textDecoration": "none"
    },
    "dropbtn": {
      "display": "block",
      "color": "white",
      "textAlign": "center",
      "padding": "14px 16px",
      "textDecoration": "none"
    },
    "menuwrapper_li_dropdown": {
      "display": "inline-block"
    },
    "menuwrapper_li_hover": {
      "background": "#AAA5A5"
    },
    "dropdown_content": {
      "display": "none",
      "position": "absolute",
      "backgroundColor": "#333333",
      "minWidth": "160px",
      "boxShadow": "0px 8px 10px 0px rgba(0,0,0,0.2)"
    },
    "dropdown_content_a": {
      "color": "white",
      "padding": "12px 16px",
      "textDecoration": "none",
      "display": "block",
      "textAlign": "left"
    },
    "dropdown_content_a_hover": {
      "background": "#AAA5A5"
    },
    "dropdown_hover__dropdown_content": {
      "display": "block"
    }
  }