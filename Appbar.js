import React from 'react'
import { slide as Menu } from 'react-burger-menu'; 

export default props =>{
              return (
                <Menu>
                  <a className="menu-item" href="/">
                    Home
                  </a>
            
                  <a className="menu-item" href="/laravel">
                    About
                  </a>
            
                  <a className="menu-item" href="/angular">
                    FAQs
                  </a>
            
                  <a className="menu-item" href="/react">
                    Contact
                  </a>
            
                  <a className="menu-item" href="/vue">
                  DropDownMenu
                  </a>
                </Menu>
              );
            };
        