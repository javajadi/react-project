import React from "react";
import TopNavbarItem from "./TopNavbarItem";

export default class TopNavbar extends React.Component {
  render() {
    return (
      <div>
        <li style={Align}>
          <TopNavbarItem
            title="Home"
            show={() => {
              this.props.history.push("/");
            }}
          />
          <TopNavbarItem
            title="Tabs"
            show={() => {
              this.props.history.push("/tabs");
            }}
          />
          <TopNavbarItem
            title="Component"
            show={() => {
              this.props.history.push("/component");
            }}
            isChild={true}
          />
        </li>
      </div>
    );
  }
}

const Align = {
  textAlign: "center",
  padding: "auto",
  marginLeft: 70,
  width: "40%",
  background: "Grey",
  display: "flex",
  float: "left",
  position: "inherit",
  listStyle: "none",
  cursor: "pointer"
};
