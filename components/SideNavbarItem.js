import React from "react";
import image from "../Asssets/img/logo23.png";
import image2 from "../Asssets/img/logo3.png";

export default class SideNavbarItem extends React.Component {
  state = {
    expand: false
  };
  click = () => {
    this.setState({ expand: !this.state.expand });
  };
  render() {
    const { show } = this.props;

    return (
      <div
        style={LiDiv}
        onClick={() => {
          show();
        }}
      >
        {this.props.title}
        {this.props.isChild ? (
          !this.state.expand ? (
            <img
              onClick={this.click}
              style={image1style}
              src={image}
              alt="some text"
            />
          ) : (
            <img
              onClick={this.click}
              style={image2style}
              src={image2}
              alt="some text"
            />
          )
        ) : null}
        {this.props.isChild && this.state.expand ? (
          <ul style={LiStyle.ul}>
            <li
              onClick={() => {
                show();
              }}
              style={LiStyle.li}
            >
              Genral Assesories
            </li>
            <li style={LiStyle.li}>Mobile Accesories</li>
            <li style={LiStyle.li}>General Electronics</li>
          </ul>
        ) : null}
      </div>
    );
  }
}
const image1style = {
  marginLeft: "8px",
  width: "15px",
  height: "10px",
  cursor: "pointer"
};
const image2style = {
  marginLeft: "8px",
  width: "15px",
  height: "10px",
  cursor: "pointer"
};
const LiDiv = {
  padding: 10
};
const LiStyle = {
  ul: {
    display: "flex",
    background: "lightGrey",
    color: "tomato",
    margin: "auto",
    padding: 8,
    width: "auto",
    height: "auto",
    flexDirection: "column",
    textAlign: "center",
    position: "relative"
  },
  li: {
    listStyleType: "none",
    padding: "6px",
    paddingLeft:10,
    textAlign: "start",
  }
};
