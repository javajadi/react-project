import React from "react";
import Menuicon from "../Asssets/img/img_106001.png";

export default class AppBar extends React.Component {
  render() {
    return (
      <div style={style}>
        <div
          onClick={() => {
            this.props.toggleMenu();
          }}
        >
          <img alt="NotFound" src={Menuicon} style={Menuiconstyl} />
        </div>
        <div style={logoName}>
          {this.props.logo}
          {this.props.appName}
        </div>
        <div style={navstyle}>
          {this.props.navBar}
          {this.props.loginInfo}
        </div>
      </div>
    );
  }
}

const Menuiconstyl = {
  width: "25px",
  height: "25px",
  position: "fixed",
  marginTop: "10px",
  marginLeft: "10px",
  marginRight: "10px",
  marginBottom: "10px",
  cursor: "pointer"
};

const style = {
  width: "100%",
  background: "#E7E7E7",
  position: "fixed",
  border: "#777"
};
const logoName = {
  float: "left",
  marginLeft: "50px"
};
const navstyle = {
  marginLeft: 15
};
