import React from "react";
import image from "../Asssets/img/logo23.png";
import image2 from "../Asssets/img/logo3.png";

export default class TopNavbarItem extends React.Component {
  state = {
    expand: false
  };
  click = () => {
    this.setState({ expand: !this.state.expand });
  };
  render() {
    const { show, title } = this.props;

    return (
      <div
        style={LiDiv}
        onClick={() => {
          show();
        }}
      >
        {title}
        {this.props.isChild ? (
          !this.state.expand ? (
            <img
              onClick={this.click}
              style={image1style}
              src={image}
              alt="some text"
            />
          ) : (
            <img
              onClick={this.click}
              style={image2style}
              src={image2}
              alt="some text"
            />
          )
        ) : null}
        {this.props.isChild && this.state.expand ? (
          <ul style={LiStyle.ul}>
            <li>Genral Assesories</li>
            <li>Mobile Accesories</li>
            <li>General Electronics</li>
          </ul>
        ) : null}
      </div>
    );
  }
}
const image1style = {
  marginLeft: "8px",
  width: "15px",
  height: "10px",
  cursor: "pointer",
};
const image2style = {
  marginLeft: "8px",
  width: "15px",
  height: "10px",
  cursor: "pointer",
};
const LiDiv = {
  padding:"10px 10px 10px 25px"
};
const LiStyle = {
  ul: {
    display: "flex",
    background: "#568966",
    color: "white",
    margin: "auto",
    padding: 8,
    width: "auto",
    height: "auto",
    flexDirection: "column",
    textAlign: "center",
    position: "fixed",
    listStyle: "none"
  }
};
