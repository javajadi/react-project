import React from "react";
export default class LoginInfo extends React.Component {
  constructor(props) {
    super(props);
    this.handleLoginClick = this.handleLoginClick.bind(this);
    this.handleLogoutClick = this.handleLogoutClick.bind(this);
    this.state = { isLoggedIn: false };
  }

  handleLoginClick() {
    this.setState({ isLoggedIn: true });
  }

  handleLogoutClick() {
    this.setState({ isLoggedIn: false });
  }

  render() {
    const isLoggedIn = this.state.isLoggedIn;
    let button;

    if (isLoggedIn) {
      button = <LogoutButton onClick={this.handleLogoutClick} />;
    } else {
      button = <LoginButton onClick={this.handleLoginClick} />;
    }

    return (
      <div>
        <div style={pStyles}>
          {" "}
          <Greeting isLoggedIn={isLoggedIn} />
        </div>

        <div style={btnstyle}>{button} </div>
      </div>
    );
  }
}

function UserGreeting(props) {
  return <h3>Javajadi@gmail.com</h3>;
}

function GuestGreeting(props) {
  return <h3>Please sign up.</h3>;
}

function Greeting(props) {
  const isLoggedIn = props.isLoggedIn;
  if (isLoggedIn) {
    return <UserGreeting />;
  }
  return <GuestGreeting />;
}

function LoginButton(props) {
  return (
    <button
      style={{ color: "#221612", background: "Grey", marginTop: "8px" }}
      onClick={props.onClick}
    >
      Login
    </button>
  );
}

function LogoutButton(props) {
  return (
    <button
      style={{ color: "#221612", background: "Grey", marginTop: "8px" }}
      onClick={props.onClick}
    >
      Logout
    </button>
  );
}

// export default class LoginInfo extends React.Component {
//   logoutHandler =(e) => {
//     this.props.history.replace('/login')
// }
//   render() {

//     return (
//       <div>
//        {/* <button
//           style={btnstyle}
//           onClick={() => {
//             this.props.click();
//             alert(this.props.isLogin);
//           }}
//         >
//           {this.props.isLogin===true ? "Login" : "Logout" }
//         </button> */}
//         <button href="#" onClick={e=>this.logoutHandler(e)}>Logout</button>
//         {!this.props.showUsername ? (
//           <p style={pStyles}>JavaIqbal@gmail.com</p>
//         ) : (
//           ""
//         )}
//       </div>
//     );
//   }
// }

const pStyles = {
  float: "left",
  position: "inherit",
  marginLeft: "50px"
};
const btnstyle = {
  float: "right",
  position: "relative",
  marginRight: 20
  // background: "Grey",
  // color: "#221612"
};
