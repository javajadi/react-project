import React, { Component } from "react";
import SideNavbarItem from "./SideNavbarItem";

export default class SideBar extends Component {
  state = {
    expand: true
  };
  render() {
    return (
      <div>
        {this.props.show ? (
          <li style={stysideBar}>
            <SideNavbarItem
              title="Home"
              show={() => {
                this.props.history.push("/");
              }}
            />
            <SideNavbarItem
              title="Tabs"
              show={() => {
                this.props.history.push("/tabs");
              }}
            />
            <SideNavbarItem
              title="Component"
              isChild={true}
              show={() => {
                this.props.history.push("/component");
              }}
            />
            <SideNavbarItem
              title="Items"
              show={() => {
                this.props.history.push("/tabs");
              }}
            />
          </li>
        ) : null}
      </div>
    );
  }
}

const stysideBar = {
  padding: "10px",
  width: "20%",
  height: "600px",
  float: "left",
  display: "block",
  background: "lightGrey",
  marginTop: "50px",
  color: "tomato",
  cursor: "pointer"
  // position: "relative"
};
