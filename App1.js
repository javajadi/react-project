import React,{ Component } from 'react';

import './App.css';

const divStyle = {
  margin: '40px',
  border: '5px solid pink'
};
const pStyle = {
  fontSize: '15px',
  textAlign: 'center'
};

const navbar = {
  overflow: 'hidden',
  backgroundColor: 333,

}

const dropdown = {
  float: 'left',
  overFlow: 'hidden',
}
// dropdown:hover ={
//   background: red
// }
const dropbtn = {
  fontSize: 16,  
  color: 225,
  padding: 14,
  background: 'inherit',
  fontFamily: 'inherit',
  border: 'none',
  textDecoration: 'none',
  textAlign: 'center',
  
}


// .navbar a:hover, .dropdown:hover .dropbtn {
//   background-color: red;
// }

 const dropdowncontent = {
  display: 'block',
  position: 'inherit',
  minWidth: 160,
  display: 'block',
  background: '#ddd',

}

 const ab = {
  float: 'none',
  color: 'black',
  padding: '12px 16px',
  textDecoration: 'none',
  display: 'block',
  textAlign: 'center',
  hover: '#000'
} 


// const ButtonClass= {
//   '&:hover': {
//        textDecoration: 'underline',
//   },       
// }

// .dropdown-content a:hover {
//   background-color: #ddd;
// }

// .dropdown:hover .dropdown-content {
//   
// }


class Car extends React.Component {
  render() {
    return <h2>I am a Car!</h2>;
  }
}

class Garage extends React.Component {
  render() {
    return (
      <div>
      <h1>Who lives in my Garage?</h1>
      <Car/>
      </div>
    );
  }
}

class Card extends Component {
  constructor() {
    super();
    
    this.state = {
      showMenu: false,
    }
    
    this.showMenu = this.showMenu.bind(this);
  }
  
  showMenu(event) {
    event.preventDefault();
    
    this.setState({
      showMenu: true,
    });
  }
  

  render() {
    return (
      
        <a onClick={this.showMenu} style={dropbtn}>
          Show Dropdown
        

        {
          this.state.showMenu
            ? (
                <ul style={dropdowncontent}>
                <li style={ab} > Menu item 1 </li>
                <li style={ab}> Menu item 2 </li>
                <li style={ab}> Menu item 3 </li>
                </ul>
            
            )
            : (
              null
            )
        }
        </a>
    
    );
  }
}




const Box = () => (
  <div>
      
    <div style={navbar}>
        <a style={dropbtn} href="#home">Home</a>
        <a style={dropbtn} href="#news">News</a>
        <a style={dropbtn} href="#contact">Contact</a>
        <a style={dropbtn} href="#about">About</a>
        <Card style={dropbtn}/>

        
          
        {/* <button style={dropbtn}>Dropdown 
          <i className="fa fa-caret-down"></i>
        </button>
        
        <div style={dropdowncontent}>
          <a style={ab} href="#">Opperation</a>
          <a style={ab} href="#">Managment</a>
          <a style={ab} href="#">Production</a> 
        </div> */}
    
      
      </div> 
      <div style={divStyle}>
        <p style={pStyle}>Get started with inline style</p>
      
      
      <Garage />
    </div>
  </div>
);

export default Box;