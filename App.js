import React from "react";
import { Router, Route } from "react-router";
import AppBar from "./components/AppBar";
import SideBar from "./components/SideBar";
import TopNavbar from "./components/TopNavbar";
import LoginInfo from "./components/LoginInfo";
import Menu from "./components/SideBar";
import image from "./Asssets/img/logo2.jpg";
import history from "./components/history";
import Home from "./Home";
import Tabs from "./About";
import Comp from "./Comp";

export default class App extends React.Component {
  state = {
    showUsername: false,
    islogin: false,
    showSide: true
  };
  logo = <img src={image} alt="some text" width="51px" height="unset" />;
  navBar = (
    <div>
      <TopNavbar history={history} />
    </div>
  );

  loginInfo = <LoginInfo />;
  sidebar = <Menu />;
  render() {
    return (
      <div>
        <Router history={history}>
          <div>
            <div>
              <AppBar
                toggleMenu={() => {
                  this.setState({ showSide: !this.state.showSide });
                }}
                isStatic={true}
                logo={this.logo}
                appName="Daraz"
                navBar={this.navBar}
                loginInfo={this.loginInfo}
              />
            </div>
            <div>
              <SideBar history={history} show={this.state.showSide} />
            </div>
          </div>

          <Route exact path="/" component={Home} />
          <Route path="/tabs" component={Tabs} />
          <Route path="/component" component={Comp} />
        </Router>
      </div>
    );
  }
}
