import React from "react";
import Tab from "./Tabs";

export default class Tabs extends React.Component {
  state = {
    showUsername: false,
    islogin: false,
    showSide: true
  };
  render() {
    return (
      <div>
        <div style={bodysty}>
          <h1>Tabs..</h1>
          <div>
            <main>
              <Tab>
                Tab 1
                <span>
                  Well here's a nested Tabs element.
                  <Tab>
                    Tab 1.1
                    <span> Content of 1.1</span>
                    Tab 1.2
                    <span>Content of 1.2</span>
                  </Tab>
                </span>
                Tab 2<span>Tab 2 Content</span>
                Tab 3<span>Tab 3 Content</span>
              </Tab>
            </main>
          </div>
        </div>
      </div>
    );
  }
}

const bodysty = {
  padding: "10px",
  width: "75%",
  float: "left",
  display: "block",
  background: "lightGrey",
  marginTop: "50px",
  marginLeft: "5px",
  color: "tomato"
  // position: "relative"
};
