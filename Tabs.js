import React from "react";

export default class Tab extends React.Component {
  constructor(props) {
    super();
    this.state = {
      active: 0
    };
  }
  select = i => {
    let _this = this;
    return function() {
      _this.setState({
        active: i
      });
    };
  };

  renderTabs = () => {
    return React.Children.map(this.props.children, (item, i) => {
      if (i % 2 === 0) {
        let active = this.state.active === i ? "active" : "";
        return (
          <a
            style={astyle}
            onClick={this.select(i)}
            className={`${active} tab`}
          >
            {item}
          </a>
        );
      }
    });
  };

  renderContent() {
    return React.Children.map(this.props.children, (item, i) => {
      if (i - 1 === this.state.active) {
        return <div style={content}>{item}</div>;
      } else {
        return;
      }
    });
  }

  render() {
    return (
      <div style={tabs}>
        {this.renderTabs()}
        {this.renderContent()}
      </div>
    );
  }
}
const tabs = {
  marginTop: "10px",
  color: "rgba(0,0,0,0.6)",
  display: "listItem",
  padding: "10px",
  borderBottom: "2px solid rgba(0,0,0,0.2)",
  borderRadius: "10px 10px 0 0",
  cursor: "pointer",
  background: "rgba(0,0,0,0.2)"
};

const content = {
  padding: "20px",
  color: "rgba(0,0,0,0.6)",
  background: "rgba(255,255,255,0.2)"
  //  margin: "inherit",
};

const astyle = {
  display: "inlineBlock",
  backgroundColor: "rgb(231, 231, 231)",
  padding: "10px",
  border: "2px solid",
  borderRadius: "8px"
};
